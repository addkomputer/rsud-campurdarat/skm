<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h5 class="m-0 text-dark"><i class="<?= @$menu['icon'] ?>"></i> <?= @$menu['menu_name'] ?></h5>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item active">Pengaturan</li>
            <li class="breadcrumb-item active"><?= @$menu['menu_name'] ?></li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div><!-- /.content-header -->

  <!-- Main content -->
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Daftar <?= $menu['menu_name'] ?></h3>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-md-12">
                  <div class="flash-success" data-flashsuccess="<?= $this->session->flashdata('flash_success') ?>"></div>
                  <div class="table-responsive">
                    <table class="table table-bordered table-sm table-head-fixed" style="width:100% !important">
                      <thead>
                        <tr>
                          <th rowspan="2" class="text-center text-middle" width="40">No.</th>
                          <th rowspan="2" class="text-center text-middle" width="400">Jenis Pelayanan</th>
                          <th colspan="<?= count($pertanyaan) ?>" class="text-center">Pertanyaan</th>
                          <th rowspan="2" class="text-center text-middle" width="80">Jumlah</th>
                        </tr>
                        <tr>
                          <?php foreach ($pertanyaan as $r) : ?>
                            <th class="text-center"><?= $r['pertanyaan_id'] ?></th>
                          <?php endforeach; ?>
                        </tr>
                      </thead>
                      <?php if (@$main == null) : ?>
                        <tbody>
                          <tr>
                            <td class="text-center" colspan="99"><i>Tidak ada data!</i></td>
                          </tr>
                        </tbody>
                      <?php else : ?>
                        <tbody>
                          <?php foreach ($main as $r) : ?>
                            <tr>
                              <td class="text-center"><?= $r['jenis_pelayanan_id'] ?></td>
                              <td class="text-left"><?= $r['jenis_pelayanan_name'] ?></td>
                              <?php $total = 0;
                              foreach ($pertanyaan as $k => $v) : ?>
                                <td class="text-center"><?= @$r['respon'][$k]['jumlah'] ?></td>
                              <?php $total += $r['respon'][$k]['jumlah'];
                              endforeach; ?>
                              <td class="text-center text-bold"><?= $total ?></td>
                            </tr>
                          <?php endforeach; ?>
                        </tbody>
                      <?php endif; ?>
                    </table>
                  </div>
                </div>
              </div><!-- /.row -->
            </div>
          </div>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </div><!-- /.content -->
</div>
<!-- /.content-wrapper -->