<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Matriks extends MY_Controller
{

  var $menu_id, $menu, $cookie;

  function __construct()
  {
    parent::__construct();

    $this->load->model(array(
      'config/m_config',
      'pertanyaan/m_pertanyaan',
      'm_matriks'
    ));

    $this->menu_id = '03.02';
    $this->menu = $this->m_config->get_menu($this->menu_id);
    if ($this->menu == null) redirect(site_url() . '/error/error_403');

    //cookie 
    $this->cookie = get_cookie_menu($this->menu_id);
    if ($this->cookie['search'] == null) $this->cookie['search'] = array('term' => '');
    if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => 'jenis_pelayanan_id', 'type' => 'asc');
    if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 10;
    if ($this->cookie['cur_page'] == null) 0;
  }

  public function index()
  {
    authorize($this->menu, '_read');
    hitung();
    //main data
    $data['menu'] = $this->menu;
    $data['main'] = $this->m_matriks->all_data();
    $data['pertanyaan'] = $this->m_matriks->pertanyaan();
    //set pagination
    set_pagination($this->menu, $this->cookie);
    //render
    $this->render('index', $data);
  }
}
