<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_matriks extends CI_Model
{

  public function all_data()
  {
    $result = array();

    $jenis_pelayanan = $this->db->query("SELECT jenis_pelayanan_id, jenis_pelayanan_name FROM jenis_pelayanan ORDER BY jenis_pelayanan_id")->result_array();
    foreach ($jenis_pelayanan as $key => $value) {
      $row = $value;
      $respon = $this->db->query(
        "SELECT a.pertanyaan_id, IFNULL(b.jumlah,0) AS jumlah
        FROM pertanyaan a
        LEFT JOIN (
          SELECT pertanyaan_id, SUM(nilai) as jumlah 
          FROM respon 
          JOIN responden ON respon.responden_id = responden.responden_id 
          WHERE 
            jenis_pelayanan_id = '" . $value['jenis_pelayanan_id'] . "'
          GROUP BY pertanyaan_id
        ) as b ON b.pertanyaan_id = a.pertanyaan_id"
      )->result_array();
      $row['respon'] = $respon;
      array_push($result, $row);
    }

    return $result;
  }

  public function pertanyaan()
  {
    return $this->db->get('pertanyaan')->result_array();
  }
}
