<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title><?= @$profile['app_name'] ?></title>
  <link rel="shortcut icon" href="<?= base_url() ?>images/logos/<?= @$profile['logo'] ?>" type="image/x-icon">
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="<?= base_url() ?>plugins/fontawesome-free/css/all.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="<?= base_url() ?>plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?= base_url() ?>plugins/select2/css/select2.css">
  <link rel="stylesheet" href="<?= base_url() ?>plugins/select2-bootstrap4-theme/select2-bootstrap.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url() ?>dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <!-- REQUIRED SCRIPTS -->

  <!-- jQuery -->
  <script src="<?= base_url() ?>plugins/jquery/jquery.min.js"></script>
  <!-- Bootstrap 4 -->
  <script src="<?= base_url() ?>plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- AdminLTE App -->
  <script src="<?= base_url() ?>dist/js/adminlte.min.js"></script>
  <!-- JQuery Validation -->
  <script src="<?= base_url() ?>plugins/jquery-validation/jquery.validate.js"></script>
  <script src="<?= base_url() ?>plugins/jquery-validation/localization/messages_id.js"></script>
  <!-- Select2 -->
  <script src="<?= base_url() ?>plugins/select2/js/select2.full.js"></script>
  <!-- ChartJS -->
  <script src="<?= base_url() ?>plugins/chart.js/Chart.min.js"></script>
  <script>
    $(document).ready(function() {
      //select2
      // $.fn.select2.defaults.set("theme", "bootstrap");
      $('.select2').select2();
      $('.select2-hidden').select2({
        minimumResultsForSearch: Infinity
      });
    })
  </script>
  <style>
    @media screen and (min-width: 0px) and (max-width: 320px) {
      #brand-subtitle {
        display: none !important
      }
    }
  </style>
</head>

<body class="hold-transition layout-top-nav">
  <div class="wrapper">
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">