<!-- Navbar -->
<nav class="main-header navbar navbar-expand-md navbar-light navbar-white">
  <div class="container">
    <a href="<?= base_url() ?>" class="navbar-brand">
      <div class="d-flex justify-content-between">
        <div class="col-md-1">
          <img src="<?= base_url() ?>images/logos/<?= @$profile['logo'] ?>" alt="<?= @$profile['app_name'] ?>" height="30">
        </div>
        <div class="col-md-11 ml-3">
          <div class="brand-text text-blue" style="line-height:1"><?= @$profile['short_name'] ?></div>
          <div id="brand-subtitle" class="brand-text text-gray" style="font-size:12px; line-height:1"><?= @$profile['app_name'] ?></div>
        </div>
      </div>
    </a>

    <button class="navbar-toggler order-1" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse order-3" id="navbarCollapse">
      <ul class="order-1 order-md-3 navbar-nav navbar-no-expand ml-auto">
        <li class="nav-item">
          <a href="<?= site_url() ?>" class="nav-link"><i class="fas fa-home"></i> Beranda</a>
        </li>
        <li class="nav-item">
          <a href="<?= site_url() ?>/front/ikm" class="nav-link"><i class="fas fa-users"></i> IKM</a>
        </li>
        <li class="nav-item">
          <a href="<?= site_url() ?>/auth/login" class="btn btn-sm btn-primary mt-1"><i class="fas fa-sign-in-alt"></i> Masuk</a>
        </li>
      </ul>
    </div>
  </div>
</nav>
<!-- /.navbar -->