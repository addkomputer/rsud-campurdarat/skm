</div>
<!-- /.content-wrapper -->
<!-- Main Footer -->
<footer class="main-footer">
  <!-- To the right -->
  <div class="float-right d-none d-sm-inline">
    Version <?= @$profile['version'] ?>
  </div>
  <!-- Default to the left -->
  <strong>Copyright &copy; <?= (date('Y') == @$profile['start_year']) ? @$profile['start_year'] : $profile['start_year'] . ' - ' . date('Y') ?> <a href="<?= @$profile['website'] ?>" target="_blank"><?= @$profile['company_name'] ?></a>.</strong> All rights reserved.
</footer>
</div>
<!-- ./wrapper -->
</body>

</html>