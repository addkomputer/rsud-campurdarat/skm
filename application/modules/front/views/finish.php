<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark"> Terimakasih</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#"><?= $all_step ?></a></li>
          <li class="breadcrumb-item active"><?= $all_step ?></li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="card" style="min-height:70vh">
          <div class="card-body">
            <p class="card-text">
            <div class="row">
              <div class="col-6">
                <img src="<?= base_url() ?>images/illustrations/terimakasih.png" alt="" width="80%">
              </div>
              <div class="col-6">
                <h2>Terimakasih</h2>
                <p>Terimakasih telah memberikan penilaian terhadap pelayanan kami. </p>
                <p>Penilaian anda menjadi acuan kami untuk meningkatkan pelayanan di <?= @$profile['company_name'] ?> menjadi lebih baik.</p>
                <br><br>
                <p>Anda akan dialihkan dalam...</p>
                <h1 id="timer"></h1>
              </div>
            </div>
            </p>

          </div>
        </div>
      </div>
    </div>
    <!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content -->

<script>
  $(document).ready(function() {
    var x = 10;
    $("#timer").html(x);
    setInterval(function() {
      if (x == 0) {
        window.location = "<?= site_url() ?>";
      }
      if (x > 0) {
        x -= 1
      }
      $("#timer").html(x);
    }, 1000);
  })
</script>