<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark"> Profil Responden</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">2</a></li>
          <li class="breadcrumb-item active"><?= $all_step ?></li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="card" style="min-height:70vh">
          <div class="card-body">
            <p class="card-text">
            <div class="row">
              <div class="col-md-6 col-sm-12">
                <img src="<?= base_url() ?>images/illustrations/identity.png" alt="" width="80%">
              </div>
              <div class="col-md-6 col-sm-12">
                <h4>Profil Responden</h4>
                <p>Isikan formulir dibawah ini sesuai dengan profil Anda.</p>
                <form id="form" action="<?= site_url() ?>/front/responden_save" method="post">
                  <input type="hidden" class="form-control form-control-sm" name="responden_id" id="responden_id" value="" readonly>
                  <input type="hidden" class="form-control" id="nama" name="nama" value="<?= guidv4() ?>" required>
                  <div class="form-group row" style="margin-bottom: 0px !important">
                    <label class="col-sm-3 col-form-label text-right">Umur <span class="text-danger">*</span></label>
                    <div class="col-sm-4">
                      <div class="input-group">
                        <input type="number" class="form-control" id="umur" name="umur" required>
                        <div class="input-group-append">
                          <span class="input-group-text">Tahun</span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group row mt-2" style="margin-bottom: 0px !important">
                    <label class="col-sm-3 col-form-label text-right">Jenis Kelamin <span class="text-danger">*</span></label>
                    <div class="col-sm-3">
                      <div class="form-group clearfix mt-2">
                        <div class="icheck-primary d-inline">
                          <input type="radio" id="jenis_kelamin_laki_laki" name="jenis_kelamin" value="0" checked="">
                          <label for="jenis_kelamin_laki_laki">
                            Laki-laki
                          </label>
                        </div>
                      </div>
                    </div>
                    <div class="col-sm-3 mt-2">
                      <div class="form-group clearfix">
                        <div class="icheck-primary d-inline">
                          <input type="radio" id="jenis_kelamin_perempuan" name="jenis_kelamin" value="1">
                          <label for="jenis_kelamin_perempuan">
                            Perempuan
                          </label>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="form-group row" style="margin-bottom: 5px !important">
                    <label class="col-sm-3 col-form-label text-right">Pendidikan <span class="text-danger">*</span></label>
                    <div class="col-sm-9">
                      <div class="row">
                        <?php foreach ($pendidikan as $p) : ?>
                          <?php foreach ($p as $r) : ?>
                            <div class="col-sm-4 mt-2">
                              <div class="form-group clearfix mb-1">
                                <div class="icheck-primary d-inline">
                                  <input type="radio" id="pendidikan_<?= @$r['pendidikan_id'] ?>" name="pendidikan_id" value="<?= @$r['pendidikan_id'] ?>" <?= ($r['pendidikan_id'] == '01') ? 'checked' : '' ?>>
                                  <label for="pendidikan_<?= @$r['pendidikan_id'] ?>">
                                    <?= $r['pendidikan_name'] ?>
                                  </label>
                                </div>
                              </div>
                            </div>
                          <?php endforeach; ?>
                        <?php endforeach; ?>
                      </div>
                    </div>
                  </div>
                  <div class="form-group row" style="margin-bottom: 0px !important">
                    <label class="col-sm-3 col-form-label text-right">Pekerjaan <span class="text-danger">*</span></label>
                    <div class="col-sm-9">
                      <div class="row">
                        <?php foreach ($pekerjaan as $p) : ?>
                          <?php foreach ($p as $r) : ?>
                            <div class="col-sm-4 mt-2">
                              <div class="form-group clearfix mb-1">
                                <div class="icheck-primary d-inline">
                                  <input type="radio" id="pekerjaan_<?= @$r['pekerjaan_id'] ?>" name="pekerjaan_id" value="<?= @$r['pekerjaan_id'] ?>" <?= ($r['pekerjaan_id'] == '01') ? 'checked' : '' ?>>
                                  <label for="pekerjaan_<?= @$r['pekerjaan_id'] ?>">
                                    <?= $r['pekerjaan_name'] ?>
                                  </label>
                                </div>
                              </div>
                            </div>
                          <?php endforeach; ?>
                        <?php endforeach; ?>
                      </div>
                    </div>
                  </div>
                  <div class="row mt-3">
                    <div class="col-md-10 offset-md-3">
                      <button type="submit" class="btn btn btn-primary btn-submit">Lanjut <i class="fas fa-arrow-alt-circle-right"></i></button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
            </p>
          </div>
        </div>
      </div>
    </div>
    <!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content -->
<script>
  $(document).ready(function() {
    $("#form").validate({
      rules: {

      },
      messages: {

      },
      errorElement: "em",
      errorPlacement: function(error, element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if ($(element).hasClass('select2')) {
          error.insertAfter(element.next(".select2-container")).addClass('mt-1');
        } else {
          error.insertAfter(element);
        }
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function(form) {
        $(".btn-submit").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $(".btn-submit").addClass('disabled');
        $(".btn-cancel").addClass('disabled');
        form.submit();
      }
    });
  })
</script>