<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark"> Selamat Datang</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">1</a></li>
          <li class="breadcrumb-item active"><?= $all_step ?></li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="card" style="min-height:50vh">
          <div class="card-body">
            <p class="card-text">
            <div class="row">
              <div class="col-6">
                <img src="<?= base_url() ?>images/illustrations/welcome.png" alt="" width="80%">
              </div>
              <div class="col-6">
                <h2>Selamat Datang</h2>
                <p>Selamat datang di <?= @$profile['app_name'] ?> . </p>
                <p>Aplikasi pengukuran secara komprehensif tingkat kepuasan masyarakat terhadap pelayanan yang diterima di <?= @$profile['company_name'] ?>.</p>
                <p>Silakan klik tombol mulai kemudian isi identitas diri dan dilanjutkan dengan menjawab pertanyaan.</p>
                <a class="btn btn-lg btn-primary mt-2" href="<?= site_url() ?>/front/responden">Mulai Sekarang <i class="fas fa-arrow-alt-circle-right"></i></a>
              </div>
            </div>
            </p>
          </div>
        </div>
      </div>
    </div>
    <br><br><br><br>
    <!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content -->