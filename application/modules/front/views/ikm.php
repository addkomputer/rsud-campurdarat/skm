<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark"> Indeks Kepuasan Masyarakat</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">

      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="card">
          <div class="card-header">
            <h3 class="card-title">Indeks Kepuasan Masyarakat</h3>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-6">
                <img src="<?= base_url() ?>images/illustrations/hasil.png" alt="" width="80%">
              </div>
              <div class="col-6">
                <div class="card bg-green" style="height: 100% !important;">
                  <!-- /.card-header -->
                  <div class="card-body text-center">
                    Total Nilai <?= @$hasil['total_nilai'] ?>
                    <br>
                    <br>
                    Nilai Kinerja
                    <h1 style="font-size:60px !important"><?= @$hasil['nilai_mutu'] ?></h1>
                    <h3><?= @$hasil['nilai_kinerja'] ?></h3>
                  </div>
                  <!-- /.card-body -->
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-4">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Jumlah Berdasar Jenis Kelamin</h3>
              </div>
              <div class="card-body">
                <div class="chart">
                  <canvas id="jkChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                </div>
              </div>
            </div>
          </div>
          <div class="col-8">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Jumlah Berdasar Pendidikan</h3>
              </div>
              <div class="card-body">
                <div class="chart">
                  <canvas id="pendidikanChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-6">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Jumlah Berdasar Umur</h3>
              </div>
              <div class="card-body">
                <div class="chart">
                  <canvas id="umurChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                </div>
              </div>
            </div>
          </div>
          <div class="col-6">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Jumlah Berdasar Pekerjaan</h3>
              </div>
              <div class="card-body">
                <div class="chart">
                  <canvas id="pekerjaanChart" style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- /.row -->
</div><!-- /.container-fluid -->
</div>
<!-- /.content -->
<script>
  $(document).ready(function() {
    var jkChartData = {
      labels: ['Jumlah'],
      datasets: [{
        label: 'Laki-laki',
        backgroundColor: '#3498db',
        borderColor: '#2980b9',
        data: [<?= $jk_laki ?>]
      }, {
        label: 'Perempuan',
        backgroundColor: '#fd79a8',
        borderColor: '#e84393',
        data: [<?= $jk_perempuan ?>]
      }, ]
    }
    var jkChartCanvas = $('#jkChart').get(0).getContext('2d')

    var jkChartOptions = {
      responsive: true,
      maintainAspectRatio: false,
      datasetFill: false,
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true
          }
        }]
      }
    }

    var jkChart = new Chart(jkChartCanvas, {
      type: 'bar',
      data: jkChartData,
      options: jkChartOptions
    })

    var dynamicColors = function() {
      var r = Math.floor(Math.random() * 255);
      var g = Math.floor(Math.random() * 255);
      var b = Math.floor(Math.random() * 255);
      return "rgb(" + r + "," + g + "," + b + ")";
    };

    //-------------
    //- Pendidikan chart
    //-------------
    var pendidikanChartData = {
      labels: ['Jumlah'],
      datasets: [
        <?php foreach ($jml_pendidikan as $key => $value) {
          echo "{\n";
          echo "label: '" . $value['pendidikan_name'] . "',\n";
          echo "backgroundColor: '#3498db',\n";
          echo "borderColor: '#2980b9',\n";
          echo "data:[" . $value['jumlah'] . "],\n";
          echo "},\n";
        } ?>
      ]
    }
    var pendidikanChartCanvas = $('#pendidikanChart').get(0).getContext('2d')

    var pendidikanChartOptions = {
      responsive: true,
      maintainAspectRatio: false,
      datasetFill: false,
      legend: false,
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true
          }
        }]
      }
    }

    var pendidikanChart = new Chart(pendidikanChartCanvas, {
      type: 'bar',
      data: pendidikanChartData,
      options: pendidikanChartOptions
    })

    //-------------
    //- Umur chart
    //-------------
    var umurChartData = {
      labels: ['Jumlah'],
      datasets: [
        <?php foreach ($jml_umur as $key => $value) {
          echo "{\n";
          echo "label: '" . $value['umur'] . " tahun',\n";
          echo "backgroundColor: '#3498db',\n";
          echo "borderColor: '#2980b9',\n";
          echo "data:[" . $value['jumlah'] . "],\n";
          echo "},\n";
        } ?>
      ]
    }
    var umurChartCanvas = $('#umurChart').get(0).getContext('2d')

    var umurChartOptions = {
      responsive: true,
      maintainAspectRatio: false,
      datasetFill: false,
      legend: false,
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true
          }
        }]
      }
    }

    var umurChart = new Chart(umurChartCanvas, {
      type: 'bar',
      data: umurChartData,
      options: umurChartOptions
    })

    //-------------
    //- Pekerjaan chart
    //-------------
    var pekerjaanChartData = {
      labels: ['Jumlah'],
      datasets: [
        <?php foreach ($jml_pekerjaan as $key => $value) {
          echo "{\n";
          echo "label: '" . $value['pekerjaan_name'] . "',\n";
          echo "backgroundColor: '#3498db',\n";
          echo "borderColor: '#2980b9',\n";
          echo "data:[" . $value['jumlah'] . "],\n";
          echo "},\n";
        } ?>
      ]
    }
    var pekerjaanChartCanvas = $('#pekerjaanChart').get(0).getContext('2d')

    var pekerjaanChartOptions = {
      responsive: true,
      maintainAspectRatio: false,
      datasetFill: false,
      legend: false,
      scales: {
        yAxes: [{
          ticks: {
            beginAtZero: true
          }
        }]
      }
    }

    var pekerjaanChart = new Chart(pekerjaanChartCanvas, {
      type: 'bar',
      data: pekerjaanChartData,
      options: pekerjaanChartOptions
    })
  })
</script>