<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark"> Saran</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#">14</a></li>
          <li class="breadcrumb-item active"><?= $all_step ?></li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="card" style="min-height:70vh">
          <div class="card-body">
            <p class="card-text">
            <div class="row">
              <div class="col-md-6 col-sm-12">
                <img src="<?= base_url() ?>images/illustrations/saran.png" alt="" width="80%">
              </div>
              <div class="col-md-6 col-sm-12">
                <h4>Kritik dan saran</h4>
                <p>Kritik dan saran anda membantu kami dalam meningkatkan pelayanan di <?= $profile['company_name'] ?></p>
                <form id="form" action="<?= site_url() ?>/front/saran_save" method="post">
                  <input type="hidden" class="form-control form-control-sm" name="responden_id" id="responden_id" value="<?= $responden_id ?>" readonly>
                  <div class="form-group row">
                    <label class="col-sm-3 col-form-label text-right">Kritik & Saran <span class="text-danger">*</span></label>
                    <div class="col-sm-9">
                      <textarea class="form-control" rows="5" id="saran" name="saran" required></textarea>
                    </div>
                  </div>
                  <div class="row mt-3">
                    <div class="col-md-10 offset-md-3">
                      <button type="submit" class="btn btn btn-primary btn-submit">Lanjut <i class="fas fa-arrow-alt-circle-right"></i></button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
            </p>
          </div>
        </div>
      </div>
    </div>
    <!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content -->
<script>
  $(document).ready(function() {
    $("#form").validate({
      rules: {

      },
      messages: {

      },
      errorElement: "em",
      errorPlacement: function(error, element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if ($(element).hasClass('select2')) {
          error.insertAfter(element.next(".select2-container")).addClass('mt-1');
        } else {
          error.insertAfter(element);
        }
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function(form) {
        $(".btn-submit").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $(".btn-submit").addClass('disabled');
        $(".btn-cancel").addClass('disabled');
        form.submit();
      }
    });
  })
</script>