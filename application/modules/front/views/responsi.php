<!-- Content Header (Page header) -->
<div class="content-header">
  <div class="container">
    <div class="row mb-2">
      <div class="col-sm-6">
        <h1 class="m-0 text-dark"> Responsi</h1>
      </div><!-- /.col -->
      <div class="col-sm-6">
        <ol class="breadcrumb float-sm-right">
          <li class="breadcrumb-item"><a href="#"><?= $pertanyaan['pertanyaan_id'] + 3 ?></a></li>
          <li class="breadcrumb-item active"><?= $all_step ?></li>
        </ol>
      </div><!-- /.col -->
    </div><!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content-header -->

<!-- Main content -->
<div class="content">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="card" style="min-height:70vh">
          <div class="card-body">
            <p class="card-text">
            <div class="row">
              <div class="col-md-6 col-sm-12">
                LOKASI PELAYANAN : <?= $responden['jenis_pelayanan_name'] ?>
                <h1><?= $pertanyaan['pertanyaan_name'] ?></h1>
              </div>
              <div class="col-md-6 col-sm-12">
                <h4>Jawaban Anda</h4>
                <br>
                <form id="form" action="<?= site_url() ?>/front/responsi_save" method="post">
                  <input type="hidden" class="form-control form-control-sm" name="responden_id" id="responden_id" value="<?= @$responden['responden_id'] ?>" readonly>
                  <input type="hidden" class="form-control form-control-sm" name="pertanyaan_id" id="pertanyaan_id" value="<?= @$pertanyaan['pertanyaan_id'] ?>" readonly>
                  <div class="list-group">
                    <?php $max_pilihan = count($pertanyaan['pilihan']) ?>
                    <?php foreach ($pertanyaan['pilihan'] as $k => $v) : ?>
                      <div class="list-group-item form-group clearfix">
                        <div class="icheck-primary d-inline">
                          <input type="radio" id="pilihan_<?= $v['pilihan_id'] ?>" name="nilai" value="<?= $v['nilai'] ?>" <?= ($k == $max_pilihan - 1) ? 'checked' : '' ?>>
                          <label for="pilihan_<?= $v['pilihan_id'] ?>" style="font-size:20px;font-weight:500 !important;width:100%">
                            <?= $v['pilihan_name'] ?>
                          </label>
                        </div>
                      </div>
                    <?php endforeach; ?>
                  </div>
                  <div class="row mt-3">
                    <div class="col-md-3 offset-md-9">
                      <button type="submit" class="btn btn-block btn-primary btn-submit">Lanjut <i class="fas fa-arrow-alt-circle-right"></i></button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
            </p>
          </div>
        </div>
      </div>
    </div>
    <!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
<!-- /.content -->
<script>
  $(document).ready(function() {
    $("#form").validate({
      rules: {

      },
      messages: {

      },
      errorElement: "em",
      errorPlacement: function(error, element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if ($(element).hasClass('select2')) {
          error.insertAfter(element.next(".select2-container")).addClass('mt-1');
        } else {
          error.insertAfter(element);
        }
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function(form) {
        $(".btn-submit").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $(".btn-submit").addClass('disabled');
        $(".btn-cancel").addClass('disabled');
        form.submit();
      }
    });
  })
</script>