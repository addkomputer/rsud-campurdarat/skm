<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Front extends FP_Controller
{

  function __construct()
  {
    parent::__construct();
    $this->load->model(array(
      'pendidikan/m_pendidikan',
      'pekerjaan/m_pekerjaan',
      'pertanyaan/m_pertanyaan',
      'respon/m_respon',
      'jenis_pelayanan/m_jenis_pelayanan',
      'm_front',
    ));
  }

  public function index()
  {
    $data['all_step'] = $this->m_pertanyaan->all_count() + 5;
    $data['gallery'] = $this->m_front->gallery();
    $this->render('index', $data);
  }

  public function ikm()
  {
    hitung();
    // $data['all_step'] = $this->m_pertanyaan->all_count() + 5;
    $data['jk_laki'] = $this->m_front->jk_laki();
    $data['jk_perempuan'] = $this->m_front->jk_perempuan();
    $data['jml_pendidikan'] = $this->m_front->jml_pendidikan();
    $data['jml_pekerjaan'] = $this->m_front->jml_pekerjaan();
    $data['jml_umur'] = $this->m_front->jml_umur();
    $data['hasil'] = $this->m_respon->hasil();
    $this->render('ikm', $data);
  }

  public function responden()
  {
    $data['all_step'] = $this->m_pertanyaan->all_count() + 5;
    $data['pendidikan'] = array_chunk($this->m_pendidikan->all_data(), 2);
    $data['pekerjaan'] = array_chunk($this->m_pekerjaan->all_data(), 2);
    $this->render('responden', $data);
  }

  public function responden_save()
  {
    $data = html_escape($this->input->post(null, true));
    $responden_id = $this->m_front->responden_save($data);
    redirect(site_url() . '/front/jenis_pelayanan/' . md5($responden_id));
  }

  public function jenis_pelayanan($responden_id = null)
  {
    if ($responden_id == null) {
      redirect(site_url());
    }
    $data['all_step'] = $this->m_pertanyaan->all_count() + 5;
    $data['responden'] = $this->m_front->responden_get($responden_id);
    $data['jenis_pelayanan'] = $this->m_front->list_jenis_pelayanan();
    $this->render('jenis_pelayanan', $data);
  }

  public function jenis_pelayanan_save()
  {
    $data = html_escape($this->input->post(null, true));
    $this->m_front->jenis_pelayanan_save($data['jenis_pelayanan_id'], $data['responden_id']);
    $first = $this->m_pertanyaan->first();
    redirect(site_url() . '/front/responsi/' . md5($data['responden_id']) . '/' . md5($first['pertanyaan_id']));
  }

  public function responsi($responden_id = null, $pertanyaan_id = null)
  {
    if ($responden_id == null || $pertanyaan_id == null) {
      redirect(site_url());
    }
    $data['all_step'] = $this->m_pertanyaan->all_count() + 5;
    $data['responden'] = $this->m_front->responden_get($responden_id);
    $data['pertanyaan'] = $this->m_front->pertanyaan_get($pertanyaan_id);
    $this->render('responsi', $data);
  }

  public function responsi_save()
  {
    $data = html_escape($this->input->post(null, true));
    $this->m_front->responsi_save($data);
    $last = $this->m_pertanyaan->last();
    if ($data['pertanyaan_id'] != $last['pertanyaan_id']) {
      $next_id = $data['pertanyaan_id'] + 1;
      $pertanyaan = $this->m_pertanyaan->by_field('pertanyaan_id', $next_id);
      redirect(site_url() . '/front/responsi/' . md5($data['responden_id']) . '/' . md5($pertanyaan['pertanyaan_id']));
    } else {
      redirect(site_url() . '/front/saran/' . $data['responden_id']);
    }
  }

  public function saran($responden_id = null)
  {
    $data['responden_id'] = $responden_id;
    $data['all_step'] = $this->m_pertanyaan->all_count() + 5;
    $this->render('saran', $data);
  }

  public function saran_save($responden_id = null)
  {
    $data = html_escape($this->input->post(null, true));
    $this->m_front->saran_save($responden_id, $data);
    redirect(site_url() . '/front/finish');
  }


  public function finish()
  {
    $data['all_step'] = $this->m_pertanyaan->all_count() + 5;
    $this->render('finish', $data);
  }
}
