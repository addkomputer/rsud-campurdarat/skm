<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_front extends CI_Model
{

  public function gallery()
  {
    return $this->db->get('gallery')->result_array();
  }

  public function list_jenis_pelayanan()
  {
    $where = "WHERE a.is_deleted = 0  AND a.is_active = 1";

    $sql = "SELECT * FROM jenis_pelayanan a $where ORDER BY created_at";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function responden_save($data)
  {
    $data['created_at'] = date('Y-m-d H:i:s');
    $data['created_by'] = 'Front';
    $this->db->insert('responden', $data);
    $insert_id = $this->db->insert_id();

    return  $insert_id;
  }

  public function responden_get($responde_id)
  {
    return $this->db->query("SELECT a.*,b.jenis_pelayanan_name FROM responden a LEFT JOIN jenis_pelayanan b ON a.jenis_pelayanan_id = b.jenis_pelayanan_id WHERE md5(a.responden_id) = '$responde_id' LIMIT 1")->row_array();
  }

  public function jenis_pelayanan_save($jenis_pelayanan_id, $responde_id)
  {
    $this->db->where('responden_id', $responde_id)->update('responden', array('jenis_pelayanan_id' => $jenis_pelayanan_id));
  }

  public function pertanyaan_get($pertanyaan_id)
  {
    $row = $this->db->query("SELECT * FROM pertanyaan WHERE md5(pertanyaan_id) = '$pertanyaan_id' LIMIT 1")->row_array();
    if ($row != null) {
      $row['pilihan'] = $this->db->query("SELECT * FROM pilihan WHERE pertanyaan_id = '".$row['pertanyaan_id']."' ORDER BY nilai ASC")->result_array();
    }
    return $row;
  }

  public function responsi_save($data)
  {
    $data['created_at'] = date('Y-m-d H:i:s');
    $data['created_by'] = 'Front';
    $this->db->insert('respon', $data);
  }

  public function saran_save($responden_id, $data)
  {
    $this->db->where('responden_id', $responden_id)->update('responden', $data);
  }

  public function jk_laki()
  {
    $data = $this->db->query("SELECT COUNT(1) as jumlah FROM responden WHERE jenis_kelamin = 0")->row_array();
    if ($data == null) {
      return 0;
    } else {
      return $data['jumlah'];
    }
  }

  public function jk_perempuan()
  {
    $data = $this->db->query("SELECT COUNT(1) as jumlah FROM responden WHERE jenis_kelamin = 1")->row_array();
    if ($data == null) {
      return 0;
    } else {
      return $data['jumlah'];
    }
  }

  public function jml_pendidikan()
  {
    $data = $this->db->query(
      "SELECT
         a.pendidikan_name, IFNULL(b.jumlah,0) as jumlah 
      FROM pendidikan a
      LEFT JOIN (
        SELECT pendidikan_id, COUNT(1) as jumlah FROM responden GROUP BY pendidikan_id
      ) as b ON b.pendidikan_id = a.pendidikan_id"
    )->result_array();
    return $data;
  }

  public function jml_umur()
  {
    $data = $this->db->query(
      "SELECT umur, COUNT(1) as jumlah FROM responden GROUP BY umur"
    )->result_array();

    return $data;
  }

  public function jml_pekerjaan()
  {
    $data = $this->db->query(
      "SELECT
         a.pekerjaan_name, IFNULL(b.jumlah,0) as jumlah 
      FROM pekerjaan a
      LEFT JOIN (
        SELECT pekerjaan_id, COUNT(1) as jumlah FROM responden GROUP BY pekerjaan_id
      ) as b ON b.pekerjaan_id = a.pekerjaan_id"
    )->result_array();
    return $data;
  }
}
