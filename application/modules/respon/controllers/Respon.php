<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Respon extends MY_Controller
{

  var $menu_id, $menu, $cookie;

  function __construct()
  {
    parent::__construct();

    $this->load->model(array(
      'config/m_config',
      'pertanyaan/m_pertanyaan',
      'm_respon'
    ));

    $this->menu_id = '03.01';
    $this->menu = $this->m_config->get_menu($this->menu_id);
    if ($this->menu == null) redirect(site_url() . '/error/error_403');

    //cookie 
    $this->cookie = get_cookie_menu($this->menu_id);
    if ($this->cookie['search'] == null) $this->cookie['search'] = array('term' => '');
    if ($this->cookie['order'] == null) $this->cookie['order'] = array('field' => 'respon_id', 'type' => 'asc');
    if ($this->cookie['per_page'] == null) $this->cookie['per_page'] = 10;
    if ($this->cookie['cur_page'] == null) 0;
  }

  public function index()
  {
    authorize($this->menu, '_read');
    hitung();
    //main data
    $data['menu'] = $this->menu;
    $data['main'] = $this->m_respon->all_data();
    $data['pertanyaan'] = $this->m_respon->pertanyaan();
    $data['hasil'] = $this->m_respon->hasil();
    $data['rata'] = $this->m_respon->rata();
    //set pagination
    set_pagination($this->menu, $this->cookie);
    //render
    $this->render('index', $data);
  }

  public function form($id = null)
  {
    ($id == null) ? authorize($this->menu, '_create') : authorize($this->menu, '_update');
    if ($id == null) {
      create_log(2, $this->menu['menu_name']);
      $data['main'] = null;
    } else {
      create_log(3, $this->menu['menu_name']);
      $data['main'] = $this->m_respon->by_field('respon_id', $id);
    }
    $data['id'] = $id;
    $data['menu'] = $this->menu;
    $this->render('form', $data);
  }

  public function save($id = null)
  {
    ($id == null) ? authorize($this->menu, '_create') : authorize($this->menu, '_update');
    $data = html_escape($this->input->post(null, true));
    if (!isset($data['is_active'])) {
      $data['is_active'] = 0;
    }
    $cek = $this->m_respon->by_field('respon_id', $data['respon_id']);
    if ($id == null) {
      if ($cek != null) {
        $this->session->set_flashdata('flash_error', 'Kode sudah ada di sistem.');
        redirect(site_url() . '/' .  $this->menu['controller']  . '/form/');
      }
      $this->m_respon->save($data, $id);
      create_log(2, $this->menu['menu_name']);
      $this->session->set_flashdata('flash_success', 'Data berhasil ditambahkan.');
    } else {
      if ($data['old'] != $data['respon_id'] && $cek != null) {
        $this->session->set_flashdata('flash_error', 'Kode sudah ada di sistem.');
        redirect(site_url() . '/' . $this->menu['controller'] . '/form/' . $id);
      }
      unset($data['old']);
      $this->m_respon->save($data, $id);
      create_log(3, $this->menu['menu_name']);
      $this->session->set_flashdata('flash_success', 'Data berhasil diubah.');
    }
    redirect(site_url() . '/' . $this->menu['controller'] . '/' . $this->menu['url'] . '/' . $this->cookie['cur_page']);
  }

  public function delete($id = null)
  {
    ($id == null) ? authorize($this->menu, '_create') : authorize($this->menu, '_update');
    $this->m_respon->delete($id);
    create_log(4, $this->menu['menu_name']);
    $this->session->set_flashdata('flash_success', 'Data berhasil dihapus.');
    redirect(site_url() . '/' . $this->menu['controller'] . '/' . $this->menu['url'] . '/' . $this->cookie['cur_page']);
  }

  public function status($type = null, $id = null)
  {
    authorize($this->menu, '_update');
    if ($type == 'enable') {
      $this->m_respon->update($id, array('is_active' => 1));
    } else {
      $this->m_respon->update($id, array('is_active' => 0));
    }
    create_log(3, $this->this->menu['menu_name']);
    redirect(site_url() . '/' . $this->menu['controller'] . '/' . $this->menu['url'] . '/' . $this->cookie['cur_page']);
  }

  public function multiple($type = null)
  {
    $data = $this->input->post(null, true);
    if (isset($data['checkitem'])) {
      foreach ($data['checkitem'] as $key) {
        switch ($type) {
          case 'delete':
            authorize($this->menu, '_delete');
            $this->m_respon->delete($key);
            $flash = 'Data berhasil dihapus.';
            $t = 4;
            break;

          case 'enable':
            authorize($this->menu, '_update');
            $this->m_respon->update($key, array('is_active' => 1));
            $flash = 'Data berhasil diaktifkan.';
            $t = 3;
            break;

          case 'disable':
            authorize($this->menu, '_update');
            $this->m_respon->update($key, array('is_active' => 0));
            $flash = 'Data berhasil dinonaktifkan.';
            $t = 3;
            break;
        }
      }
    }
    create_log($t, $this->menu['menu_name']);
    $this->session->set_flashdata('flash_success', $flash);
    redirect(site_url() . '/' . $this->menu['controller'] . '/' . $this->menu['url'] . '/' . $this->cookie['cur_page']);
  }

  public function authorization($id = null)
  {
    ($id == null) ? authorize($this->menu, '_create') : authorize($this->menu, '_update');
    if ($id == null) {
      create_log(2, $this->menu['menu_name']);
      $data['main'] = null;
    } else {
      create_log(3, $this->menu['menu_name']);
      $data['main'] = $this->m_respon->by_field('respon_id', $id);
    }
    $data['id'] = $id;
    $data['menu'] = $this->menu;
    $data['menu_list'] = $this->m_respon->menu_list($id);
    $this->render('authorization', $data);
  }

  public function authorization_save($id)
  {
    $data = html_escape($this->input->post());
    $this->m_respon->authorization_save($data, $id);
    $this->session->set_flashdata('flash_success', "Data berhasil disimpan");
    redirect(site_url() . '/' . $this->menu['controller'] . '/' . $this->menu['url'] . '/' . $this->cookie['cur_page']);
  }

  public function ajax($type = null, $id = null)
  {
    if ($type == 'check_id') {
      $data = $this->input->post();
      $cek = $this->m_respon->by_field('respon_id', $data['respon_id']);
      if ($id == null) {
        echo ($cek != null) ? 'false' : 'true';
      } else {
        echo ($id != $data['respon_id'] && $cek != null) ? 'false' : 'true';
      }
    }
  }
  function cetak_excel()
  {
    hitung();
    ini_set('memory_limit', '-1');

    //Configuration -------------------------------------------------------------------------
    $this->load->file(APPPATH . 'libraries/PHPExcel.php');
    $master_cetak = BASEPATH . 'master_cetak/respon.xlsx';
    $PHPExcel = PHPExcel_IOFactory::load($master_cetak);

    // sheet 1
    $PHPExcel->setActiveSheetIndex(0);

    //align center
    $align_center = array(
      'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
      )
    );
    //align left
    $align_left = array(
      'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT,
      )
    );
    //align right
    $align_right = array(
      'alignment' => array(
        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
      )
    );
    //font bold
    $font_bold = array(
      'font' => array(
        'bold'  => 'bold'
      )
    );
    //font underline
    $font_underline = array(
      'font' => array(
        'underline'  => 'underline'
      )
    );
    //font size 12
    $font_size_12 = array(
      'font' => array(
        'size'  => 12
      )
    );
    //wrap text
    $wrap_text = array(
      'alignment' => array(
        'wrap' => 'wrap',
      )
    );
    //create border
    $styleArray = array(
      'borders' => array(
        'allborders' => array(
          'style' => PHPExcel_Style_Border::BORDER_THIN,
          'color' => array('rgb' => '111111'),
        ),
      ),
      'font' => array(
        'size'  => 11,
        'name'  => 'Arial'
      ),
    );
    //
    //Header
    $pertanyaan = $this->m_respon->pertanyaan();
    $jml_pertanyaan = count($pertanyaan);
    //
    $PHPExcel->getActiveSheet()->setCellValue("A1", 'Tabulasi Sistem Kepuasan Masyarakat');
    //merge
    $s = 'A';
    foreach ($pertanyaan as $key => $value) {
      $s++;
    }
    $PHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(15);
    $PHPExcel->getActiveSheet()->mergeCells("A3:A4");
    $PHPExcel->getActiveSheet()->setCellValue("A3", 'No Responden');
    $PHPExcel->getActiveSheet()->mergeCells("B3:" . $s . "3");
    $PHPExcel->getActiveSheet()->setCellValue("B3", 'Nilai Unsur Pertanyaan');
    $s = 'A';
    foreach ($pertanyaan as $key => $value) {
      $s++;
      $PHPExcel->getActiveSheet()->setCellValue($s . "4", "U" . $value['pertanyaan_id']);
    }


    $PHPExcel->getActiveSheet()->getStyle("A3:" . $s . "4")->applyFromArray($styleArray);
    $PHPExcel->getActiveSheet()->getStyle("A3:" . $s . "4")->applyFromArray($font_bold);
    $PHPExcel->getActiveSheet()->getStyle("A3:" . $s . "4")->applyFromArray($align_center);
    $PHPExcel->getActiveSheet()->getStyle("A3:" . $s . "4")->applyFromArray($wrap_text);

    $main = $this->m_respon->all_data();
    $i = 5;
    // var_dump($main);
    // die;
    foreach ($main as $k => $v) {
      $PHPExcel->getActiveSheet()->setCellValue("A$i", $v['responden_id']);
      $j = 'A';
      foreach ($v['respon'] as $k2 => $v2) {
        $j++;
        $PHPExcel->getActiveSheet()->setCellValue($j . $i, $v2);
      }
      $i++;
    }
    $PHPExcel->getActiveSheet()->setCellValue("A$i", 'Σ Nilai');
    $j = 'A';
    foreach ($pertanyaan as $k => $v) {
      $j++;
      $PHPExcel->getActiveSheet()->setCellValue($j . $i, @round($v['jumlah'], 2));
    }
    $i++;
    $PHPExcel->getActiveSheet()->setCellValue("A$i", 'NRR');
    $j = 'A';
    foreach ($pertanyaan as $k => $v) {
      $j++;
      $PHPExcel->getActiveSheet()->setCellValue($j . $i, @round($v['rata_rata'], 2));
    }
    $i++;
    $PHPExcel->getActiveSheet()->setCellValue("A$i", 'NRR Trtmbng');
    $j = 'A';
    foreach ($pertanyaan as $k => $v) {
      $j++;
      $PHPExcel->getActiveSheet()->setCellValue($j . $i, @round($v['nilai_index'], 2));
    }

    $PHPExcel->getActiveSheet()->getStyle("A5:" . $s . $i)->applyFromArray($styleArray);
    $PHPExcel->getActiveSheet()->getStyle("A5:" . $s . $i)->applyFromArray($align_center);
    $PHPExcel->getActiveSheet()->getStyle("A5:" . $s . $i)->applyFromArray($wrap_text);

    $hasil = $this->m_respon->hasil();
    $PHPExcel->getActiveSheet()->setCellValue(++$j . $i, "*) " . @round($hasil['total_index'], 2));
    $PHPExcel->getActiveSheet()->getStyle("A" . $i . ":" . $j . $i)->applyFromArray($styleArray);
    $PHPExcel->getActiveSheet()->getStyle("A" . $i . ":" . $j . $i)->applyFromArray($align_center);
    $PHPExcel->getActiveSheet()->getStyle("A" . $i . ":" . $j . $i)->applyFromArray($font_bold);
    $PHPExcel->getActiveSheet()->getStyle("A" . $i . ":" . $j . $i)->applyFromArray($wrap_text);
    //
    $PHPExcel->getActiveSheet()->setCellValue("A" . ++$i, "IKM Unit Pelayanan");
    $PHPExcel->getActiveSheet()->setCellValue($j . $i, "**) " . @round($hasil['total_nilai'], 2));
    $PHPExcel->getActiveSheet()->getStyle("A" . $i . ":" . $j . $i)->applyFromArray($styleArray);
    $PHPExcel->getActiveSheet()->getStyle("A" . $i . ":" . $j . $i)->applyFromArray($align_center);
    $PHPExcel->getActiveSheet()->getStyle("A" . $i . ":" . $j . $i)->applyFromArray($font_bold);
    $PHPExcel->getActiveSheet()->getStyle("A" . $i . ":" . $j . $i)->applyFromArray($wrap_text);
    // $i++;
    $li = $i + 3;
    $PHPExcel->getActiveSheet()->setCellValue("A" . $li, "U1-U" . count($pertanyaan));
    $PHPExcel->getActiveSheet()->setCellValue("B" . $li, "`= Unsur Layanan");

    $PHPExcel->getActiveSheet()->setCellValue("A" . ($li + 1), "NRR");
    $PHPExcel->getActiveSheet()->setCellValue("B" . ($li + 1), ": Nilai Rata Rata");

    $PHPExcel->getActiveSheet()->setCellValue("A" . ($li + 2), "IKM");
    $PHPExcel->getActiveSheet()->setCellValue("B" . ($li + 2), ": Indeks Kepuasan Masyarakat");

    $PHPExcel->getActiveSheet()->setCellValue("A" . ($li + 3), "*)");
    $PHPExcel->getActiveSheet()->setCellValue("B" . ($li + 3), ": Jumlah NRR Tertimbang");

    $PHPExcel->getActiveSheet()->setCellValue("A" . ($li + 4), "**)");
    $PHPExcel->getActiveSheet()->setCellValue("B" . ($li + 4), ": Nilai NRR Tertimbang * 25");

    $PHPExcel->getActiveSheet()->setCellValue("A" . ($li + 6), "NRR");
    $PHPExcel->getActiveSheet()->setCellValue("B" . ($li + 6), ": Jumlah Nilai / Jumlah Kuisioner Terisi");

    $PHPExcel->getActiveSheet()->setCellValue("A" . ($li + 7), "NRR Tertimbang");
    $PHPExcel->getActiveSheet()->setCellValue("B" . ($li + 7), ": NRR * 0.1");

    $PHPExcel->getActiveSheet()->setCellValue("F" . $li, "No");
    $PHPExcel->getActiveSheet()->mergeCells("G" . $li . ":" . "K" . $li);
    $PHPExcel->getActiveSheet()->setCellValue("G" . $li, "Unsur Pertanyaan");
    $PHPExcel->getActiveSheet()->setCellValue("L" . $li, "Rata-rata");
    $PHPExcel->getActiveSheet()->getStyle("F" . $li . ":" . "L" . $li)->applyFromArray($font_bold);
    $li++;
    foreach ($pertanyaan as $k => $v) {
      $PHPExcel->getActiveSheet()->setCellValue("F" . ($li + $k), $v['pertanyaan_id']);
      $PHPExcel->getActiveSheet()->mergeCells("G" . ($li + $k) . ":" . "K" . ($li + $k));
      $PHPExcel->getActiveSheet()->setCellValue("G" . ($li + $k), $v['singkatan']);
      $PHPExcel->getActiveSheet()->setCellValue("L" . ($li + $k), $v['rata_rata']);
    }
    $PHPExcel->getActiveSheet()->getStyle("F" . ($li - 1) . ":" . "L" . ($li + count($pertanyaan) - 1))->applyFromArray($styleArray);
    // $PHPExcel->getActiveSheet()->setCellValue("A4", 'Tabulasi Sistem Kepuasan Masyarakat');
    // $PHPExcel->getActiveSheet()->setCellValue("A5", 'No Responden');
    // if (@$this->cookie['search']['jenisreg_st'] != '') {
    //   if (@$this->cookie['search']['lokasi_id'] != '') {
    //     $PHPExcel->getActiveSheet()->setCellValue("A3", strtoupper(get_parameter_value('jenisreg_st', $this->cookie['search']['jenisreg_st'])) . ' - ' . strtoupper($get_lokasi['lokasi_nm']));
    //   } else {
    //     $PHPExcel->getActiveSheet()->setCellValue("A3", strtoupper(get_parameter_value('jenisreg_st', $this->cookie['search']['jenisreg_st'])));
    //   }
    // } else {
    //   $PHPExcel->getActiveSheet()->setCellValue("A3", 'SEMUA LOKASI REGISTRASI');
    // }

    // Body
    // $i = 7;
    // $no = 1;
    // $tot_harga_beli = 0;
    // $tot_harga_jual = 0;
    // $tot_laba = 0;
    // foreach ($main as $row) {
    //   $tot_harga_beli += $row['harga_beli'];
    //   $tot_harga_jual += $row['harga_jual'];
    //   $tot_laba += $row['laba'];
    //   //
    //   $PHPExcel->getActiveSheet()->setCellValue("A$i", $no++);
    //   $PHPExcel->getActiveSheet()->setCellValue("B$i", $row['obat_nm']);
    //   $abj = 'C';
    //   for ($for_i = 1; $for_i <= 31; $for_i++) {
    //     $num = $for_i;
    //     $PHPExcel->getActiveSheet()->setCellValue("$abj$i", ' ' . num_id($row['tgl_' . $num]));
    //     $abj++;
    //   }
    //   $PHPExcel->getActiveSheet()->setCellValue("AH$i", ' ' . num_id($row['total']));
    //   $PHPExcel->getActiveSheet()->setCellValue("AI$i", ' ' . num_id($row['harga_beli']));
    //   $PHPExcel->getActiveSheet()->setCellValue("AJ$i", ' ' . num_id($row['harga_jual']));
    //   $PHPExcel->getActiveSheet()->setCellValue("AK$i", ' ' . num_id($row['laba']));
    //   //
    //   $i++;
    // }

    // // Total
    // $PHPExcel->getActiveSheet()->setCellValue("A$i", 'TOTAL');
    // $PHPExcel->getActiveSheet()->getStyle("A$i")->applyFromArray($font_bold);
    // $PHPExcel->getActiveSheet()->getStyle("A$i")->applyFromArray($align_center);
    // $PHPExcel->getActiveSheet()->mergeCells("A$i:AH$i");

    // $PHPExcel->getActiveSheet()->setCellValue("AI$i", 'Rp. ' . num_id($tot_harga_beli));
    // $PHPExcel->getActiveSheet()->getStyle("AI$i")->applyFromArray($font_bold);
    // $PHPExcel->getActiveSheet()->getStyle("AI$i")->applyFromArray($align_right);

    // $PHPExcel->getActiveSheet()->setCellValue("AJ$i", 'Rp. ' . num_id($tot_harga_jual));
    // $PHPExcel->getActiveSheet()->getStyle("AJ$i")->applyFromArray($font_bold);
    // $PHPExcel->getActiveSheet()->getStyle("AJ$i")->applyFromArray($align_right);

    // $PHPExcel->getActiveSheet()->setCellValue("AK$i", 'Rp. ' . num_id($tot_laba));
    // $PHPExcel->getActiveSheet()->getStyle("AK$i")->applyFromArray($font_bold);
    // $PHPExcel->getActiveSheet()->getStyle("AK$i")->applyFromArray($align_right);

    // // Creating Border
    // $last_cell = "AK" . ($i);
    // $PHPExcel->getActiveSheet()->getStyle("A6:$last_cell")->applyFromArray($styleArray);

    // Save it as file ------------------------------------------------------------------
    $file_export = 'Respon-' . date("YmdHis");
    //
    ob_end_clean();
    header('Content-Type: application/vnd.ms-excel2007');
    header('Content-Disposition: attachment; filename="' . $file_export . '.xlsx"');
    $objWriter = PHPExcel_IOFactory::createWriter($PHPExcel, 'Excel2007');
    // $objWriter->save('php://output');
    $objWriter->save(FCPATH . 'tmp/' . $file_export . '.xlsx');
    redirect(base_url() . '/tmp/' . $file_export . '.xlsx');
    //-----------------------------------------------------------------------------------
  }
}
