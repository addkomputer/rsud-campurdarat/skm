<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_respon extends CI_Model
{

  public function all_data()
  {
    $sql = "SELECT * FROM responden a ORDER BY created_at";
    $data = $this->db->query($sql)->result_array();
    $pertanyaan = $this->db->get('pertanyaan')->result_array();
    foreach ($data as $k => $v) {
      $row = array();
      foreach ($pertanyaan as $k2 => $v2) {
        $respon = $this->db->where('responden_id', $v['responden_id'])->where('pertanyaan_id', $v2['pertanyaan_id'])->get('respon')->row_array();
        if ($respon == null) {
          array_push($row, "-");
        } else {
          array_push($row, $respon['nilai']);
        }
      }
      $data[$k]['respon'] = $row;
      // $data[$k]['respon'] = $this->db->where('responden_id', $v['responden_id'])->group_by('pertanyaan_id')->get('respon')->result_array(); lkajsdlf
    }
    return $data;
  }

  public function pertanyaan()
  {
    $pertanyaan = $this->db->get('pertanyaan')->result_array();
    return $pertanyaan;
  }

  public function hasil()
  {
    $hasil = $this->db->get('hasil')->row_array();
    return $hasil;
  }

  public function rata()
  {
    $data = $this->db->query(
      "SELECT
         a.pertanyaan_id, a.pertanyaan_name, IFNULL(b.rata,0) as rata 
      FROM pertanyaan a
      LEFT JOIN (
        SELECT pertanyaan_id, AVG(nilai) as rata FROM respon GROUP BY pertanyaan_id
      ) as b ON b.pertanyaan_id = a.pertanyaan_id"
    )->result_array();
    return $data;
  }
}
