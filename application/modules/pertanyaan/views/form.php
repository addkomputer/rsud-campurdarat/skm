<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-0">
        <div class="col-sm-6">
          <h5 class="m-0 text-dark"><i class="<?= @$menu['icon'] ?>"></i> <?= @$menu['menu_name'] ?></h5>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item active">Pengaturan</li>
            <li class="breadcrumb-item active"><?= @$menu['menu_name'] ?></li>
            <li class="breadcrumb-item active"><?= ($id == null) ? 'Tambah' : 'Ubah'; ?></li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div><!-- /.content-header -->
  <!-- Main content -->
  <div class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Form <?= $menu['menu_name'] ?></h3>
            </div>
            <form id="form" action="<?= site_url() . '/' . $menu['controller'] . '/save/' . $id ?>" method="post" autocomplete="off">
              <div class="card-body">
                <div class="flash-error" data-flasherror="<?= $this->session->flashdata('flash_error') ?>"></div>
                <?php if ($id != null) : ?>
                  <input type="hidden" class="form-control form-control-sm" name="old" id="old" value="<?= @$main['pertanyaan_id'] ?>" required>
                <?php endif; ?>
                <div class="form-group row">
                  <label for="menu" class="col-sm-2 col-form-label text-right">Kode <span class="text-danger">*</span></label>
                  <div class="col-sm-2">
                    <input type="number" class="form-control form-control-sm" name="pertanyaan_id" id="pertanyaan_id" value="<?= @$main['pertanyaan_id'] ?>" required>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="icon" class="col-sm-2 col-form-label text-right">Pertanyaan <span class="text-danger">*</span></label>
                  <div class="col-sm-6">
                    <textarea class="form-control form-control-sm" name="pertanyaan_name" id="pertanyaan_name" rows="3" required><?= @$main['pertanyaan_name'] ?></textarea>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="icon" class="col-sm-2 col-form-label text-right">Singkatan <span class="text-danger">*</span></label>
                  <div class="col-sm-6">
                    <input type="text" class="form-control form-control-sm" name="singkatan" id="singkatan" value="<?= @$main['singkatan'] ?>" required>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="icon" class="col-sm-2 col-form-label text-right">Pilihan <span class="text-danger">*</span></label>
                  <div class="col-sm-5">
                    <table class="table table-sm table-bordered table-striped">
                      <thead>
                        <tr>
                          <th class="text-center">Pilihan</th>
                          <th class="text-center" width="90">Nilai</th>
                          <th class="text-center" width="60">Aksi</th>
                        </tr>
                      </thead>
                      <tbody id="pilihan-container">
                        <?php if ($id == null) : ?>
                          <tr class="pilihan-row">
                            <td><input type="text" class="form-control form-control-sm" name="pilihan_name[]" placeholder="Pilihan" value=""></td>
                            <td><input type="text" class="form-control form-control-sm" name="nilai[]" placeholder="Nilai" value=""></td>
                            <td class="text-center"><button type="button" class="btn btn-sm btn-danger pilihan-delete"><i class="fas fa-trash-alt"></i></button></td>
                          </tr>
                        <?php else : ?>
                          <?php foreach ($main['pilihan'] as $r) : ?>
                            <tr class="pilihan-row">
                              <td><input type="text" class="form-control form-control-sm" name="pilihan_name[]" placeholder="Pilihan" value="<?= $r['pilihan_name'] ?>"></td>
                              <td><input type="text" class="form-control form-control-sm" name="nilai[]" placeholder="Nilai" value="<?= $r['nilai'] ?>"></td>
                              <td class="text-center"><button type="button" class="btn btn-sm btn-danger pilihan-delete"><i class="fas fa-trash-alt"></i></button></td>
                            </tr>
                          <?php endforeach; ?>
                        <?php endif; ?>
                      </tbody>
                    </table>
                    <button type="button" class="btn btn-xs btn-default mt-2" onclick="pilihan_add()"><i class="fas fa-plus"></i> Tambah Pilihan</button>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="url" class="col-sm-2 col-form-label text-right">Aktif</label>
                  <div class="col-sm-3">
                    <div class="pretty p-icon mt-2">
                      <input class="icheckbox" type="checkbox" name="is_active" id="is_active" value="1" <?php if (@$main) {
                                                                                                            echo (@$main['is_active']) ? 'checked' : '';
                                                                                                          } else {
                                                                                                            echo 'checked';
                                                                                                          } ?>>
                      <div class="state">
                        <i class="icon fas fa-check"></i><label></label>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="card-footer">
                <div class="row">
                  <div class="col-md-10 offset-md-2">
                    <button type="submit" class="btn btn-sm btn-primary btn-submit"><i class="fas fa-save"></i> Simpan</button>
                    <a class="btn btn-sm btn-default btn-cancel" href="<?= site_url() . '/' . $menu['controller'] . '/' . $menu['url'] ?>"><i class="fas fa-times"></i> Batal</a>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </div><!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script>
  $(document).ready(function() {
    $("#form").validate({
      rules: {
        pertanyaan_id: {
          remote: {
            type: 'post',
            url: "<?= site_url() . '/' . $menu['controller'] . '/ajax/check_id/' . $id ?>",
            data: {
              'pertanyaan_id': function() {
                return $('#pertanyaan_id').val();
              }
            },
            dataType: 'json'
          }
        },
        "pilihan_name[]": {
          required: true,
        },
        "nilai[]": {
          required: true,
        }
      },
      messages: {
        pertanyaan_id: {
          remote: "Kode sudah digunakan"
        }
      },
      errorElement: "em",
      errorPlacement: function(error, element) {
        error.addClass("invalid-feedback");
        if (element.prop("type") === "checkbox") {
          error.insertAfter(element.next("label"));
        } else if ($(element).hasClass('select2')) {
          error.insertAfter(element.next(".select2-container")).addClass('mt-1');
        } else {
          error.insertAfter(element);
        }
      },
      highlight: function(element, errorClass, validClass) {
        $(element).addClass("is-invalid").removeClass("is-valid");
      },
      unhighlight: function(element, errorClass, validClass) {
        $(element).addClass("is-valid").removeClass("is-invalid");
      },
      submitHandler: function(form) {
        $(".btn-submit").html('<i class="fas fa-spin fa-spinner"></i> Proses');
        $(".btn-submit").addClass('disabled');
        $(".btn-cancel").addClass('disabled');
        form.submit();
      }
    });
  })

  function pilihan_delete(e) {
    var tr = $(e).closest("tr");
    tr.remove();
  }

  function pilihan_add() {
    const html = '<tr class="pilihan-row">' +
      '<td><input type="text" class="form-control form-control-sm" name="pilihan_name[]" placeholder="Pilihan" value="" required></td>' +
      '<td><input type="text" class="form-control form-control-sm" name="nilai[]" placeholder="Nilai" value="" required></td>' +
      '<td class="text-center"><button type="button" class="btn btn-sm btn-danger" onclick="pilihan_delete(this)"><i class="fas fa-trash-alt"></i></button></td>' +
      '</tr>';
    $("#pilihan-container").append(html);
  }
</script>