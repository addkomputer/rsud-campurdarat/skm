<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_pertanyaan extends CI_Model
{

  public function where($cookie)
  {
    $where = "WHERE a.is_deleted = 0 ";
    if (@$cookie['search']['term'] != '') {
      $where .= "AND a.pertanyaan_name LIKE '%" . $this->db->escape_like_str($cookie['search']['term']) . "%' ";
    }
    return $where;
  }

  public function list_data($cookie)
  {
    $where = $this->where($cookie);
    $sql = "SELECT * FROM pertanyaan a 
      $where
      ORDER BY "
      . $cookie['order']['field'] . " " . $cookie['order']['type'] .
      " LIMIT " . $cookie['cur_page'] . "," . $cookie['per_page'];
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_data()
  {
    $where = "WHERE a.is_deleted = 0 ";

    $sql = "SELECT * FROM pertanyaan a $where ORDER BY created_at";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  public function all_rows($cookie)
  {
    $where = $this->where($cookie);

    $sql = "SELECT COUNT(1) as total FROM pertanyaan a $where";
    $query = $this->db->query($sql);
    return $query->row_array()['total'];
  }

  public function all_count()
  {
    $sql = "SELECT COUNT(1) as total FROM pertanyaan a";
    $query = $this->db->query($sql);
    return $query->row_array()['total'];
  }

  function by_field($field, $val)
  {
    $sql = "SELECT * FROM pertanyaan WHERE $field = ?";
    $query = $this->db->query($sql, array($val));
    $row = $query->row_array();
    $row['pilihan'] = $this->pilihan($row['pertanyaan_id']);
    return $row;
  }

  public function pilihan($pertanyaan_id)
  {
    return $this->db->where('pertanyaan_id', $pertanyaan_id)->order_by('nilai')->get('pilihan')->result_array();
  }

  function first()
  {
    $sql = "SELECT * FROM pertanyaan ORDER BY pertanyaan_id ASC LIMIT 1";
    $query = $this->db->query($sql);
    $row = $query->row_array();
    return $row;
  }

  function last()
  {
    $sql = "SELECT * FROM pertanyaan ORDER BY pertanyaan_id DESC LIMIT 1";
    $query = $this->db->query($sql);
    $row = $query->row_array();
    return $row;
  }

  public function save($data, $id = null)
  {
    $d = $data;
    unset($data['pilihan_name'], $data['nilai']);
    if ($id == null) {
      $data['created_at'] = date('Y-m-d H:i:s');
      $data['created_by'] = $this->session->userdata('user_fullname');
      $this->db->insert('pertanyaan', $data);
    } else {
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('user_fullname');
      $this->db->where('pertanyaan_id', $id)->update('pertanyaan', $data);
    }

    //delete all pilihan
    $this->db->where('pertanyaan_id', $data['pertanyaan_id'])->delete('pilihan');
    foreach ($d['pilihan_name'] as $k => $v) {
      $dpilihan = array(
        'pertanyaan_id' => $data['pertanyaan_id'],
        'pilihan_id' => $k,
        'pilihan_name' => $d['pilihan_name'][$k],
        'nilai' => $d['nilai'][$k],
        'created_at' => date('Y-m-d H:i:s'),
        'created_by' => $this->session->userdata('user_fullname'),
      );
      $this->db->insert('pilihan', $dpilihan);
    }
  }

  public function update($id, $data)
  {
    $data['updated_at'] = date('Y-m-d H:i:s');
    $data['updated_by'] = $this->session->userdata('fullname');
    $this->db->where('pertanyaan_id', $id)->update('pertanyaan', $data);
  }

  public function delete($id, $permanent = true)
  {
    if ($permanent) {
      $this->db->where('pertanyaan_id', $id)->delete('pertanyaan');
    } else {
      $data['is_deleted'] = 1;
      $data['updated_at'] = date('Y-m-d H:i:s');
      $data['updated_by'] = $this->session->userdata('user_fullname');
      $this->db->where('pertanyaan_id', $id)->update('pertanyaan', $data);
    }
  }
}
